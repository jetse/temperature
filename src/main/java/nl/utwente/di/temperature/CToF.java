package nl.utwente.di.temperature;

public class CToF {
    double getInFahrenheit(String celsiusString) {
        var celsius = Double.parseDouble(celsiusString);
        return (celsius * 9 / 5) + 32;
    }
}
