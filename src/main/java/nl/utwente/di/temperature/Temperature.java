package nl.utwente.di.temperature;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Temperature extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CToF CToF;
	
    public void init() throws ServletException {
    	CToF = new CToF();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Temperature";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit: " +
                   Double.toString(CToF.getInFahrenheit(request.getParameter("celsius"))) +
                "</BODY></HTML>");
  }
  

}
