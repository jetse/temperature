package nl.utwente.di.temperature;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCToF {
    @Test
    public void testConversion() {
        CToF cToF = new CToF();
        assertEquals(32, cToF.getInFahrenheit("0"));
        assertEquals(50, cToF.getInFahrenheit("10"));
        assertEquals(68, cToF.getInFahrenheit("20"));
    }
}
